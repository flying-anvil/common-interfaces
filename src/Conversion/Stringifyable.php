<?php

/**
 * @noinspection PhpLanguageLevelInspection
 * @noinspection PhpElementIsNotAvailableInCurrentPhpVersionInspection
 */

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\Conversion;

use JetBrains\PhpStorm\Deprecated;

if (interface_exists(\Stringable::class)) {
    #[Deprecated('PHP 8 has a native interface', \Stringable::class)]
    interface Stringifyable extends \Stringable
    {
        public function __toString(): string;
    }
} else {
    interface Stringifyable
    {
        public function __toString(): string;
    }
}
