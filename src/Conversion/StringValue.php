<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\Conversion;

interface StringValue
{
    public function toString(): string;
}
