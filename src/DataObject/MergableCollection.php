<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\DataObject;

interface MergableCollection extends Collection
{
    public function merge(iterable $other);
}
