<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\DataObject;

use JsonSerializable;

interface DataObject extends JsonSerializable
{
}
