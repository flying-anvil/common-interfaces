<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\Test\DataObject;

use FlyingAnvil\Fileinfo\Test\DataObject\Implementation\CollectionImplementation;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \FlyingAnvil\Fileinfo\DataObject\Collection
 * @usesDefaultClass \FlyingAnvil\Fileinfo\DataObject\Collection
 */
class CollectionTest extends TestCase
{
    public function testCollection(): void
    {
        $instance = CollectionImplementation::createFrom([]);

        self::assertCount(0, $instance);
    }
}
