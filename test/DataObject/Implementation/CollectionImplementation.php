<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\Test\DataObject\Implementation;

use FlyingAnvil\Fileinfo\DataObject\Collection;

class CollectionImplementation implements Collection
{
    private function __construct()
    {
    }

    public static function createFrom(array $data): self
    {
        $me = new self();

        foreach ($data as $key => $value) {
            $me->add($key, $value);
        }

        return $me;
    }

    public function add($value, $key)
    {
        // TODO: Implement add() method.
    }

    public function remove(string $key, $value)
    {
        // TODO: Implement remove() method.
    }

    public function get(string $key)
    {
        // TODO: Implement get() method.
    }

    public function jsonSerialize()
    {
        // TODO: Implement jsonSerialize() method.
    }

    public function count(): int
    {
        return 0;
    }
}
