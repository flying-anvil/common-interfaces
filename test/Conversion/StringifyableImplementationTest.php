<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\Test\Conversion;

use FlyingAnvil\Fileinfo\Test\Conversion\Implementation\StringifyableImplementation;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \FlyingAnvil\Fileinfo\Conversion\Stringifyable
 * @usesDefaultClass \FlyingAnvil\Fileinfo\Conversion\Stringifyable
 */
class StringifyableImplementationTest extends TestCase
{
    public function testStringifyable(): void
    {
        $instance = new StringifyableImplementation();
        self::assertSame(StringifyableImplementation::class, (string)$instance);
    }
}
