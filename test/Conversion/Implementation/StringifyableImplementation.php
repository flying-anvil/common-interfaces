<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\Test\Conversion\Implementation;

use FlyingAnvil\Fileinfo\Conversion\Stringifyable;

class StringifyableImplementation implements Stringifyable
{
    public function __toString(): string
    {
        return __CLASS__;
    }
}
